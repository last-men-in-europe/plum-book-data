# Extracted Plum Book Data

### What is a Plum Book?
Every four years the United States' _General Publishing Offices_ publishes a book containing all federal positions.

Some of these are presidential appointees; all are Senior Executive Service. 

### What is the Data?
The `.csv` files below contain the extracted data from the following Plum Books:

* 1996
* 2004
* 2008
* 2012
* 2016

`all-data.csv` and `all-data.xlsx` is the data from all years merged together in either `.xlsx` or `.csv` format.

### What do the Codes Mean
The codes (eg for Tenure or Pay Plan) are extracted from the original tables in the Plum Books. See the Plum Books for reference.

### Additional Columns
The original columns of the Plum Book are all preserved in this extracted data with the addition of `year`, `agency` and `coordinates`. The offices have been translated into `big_office` and `little_office`.

#### `big_office`
A big office within an agency. The FBI is a `big_office` within the Department of Justice `agency`.

#### `little_office`
Sub-office of `big_office` if present. eg. _Office of Civil Rights_ is a `little_office` within the _OFFICE OF THE INSPECTOR GENERAL_ `big_office` within the _DEPARTMENT OF AGRICULTURE_ `agency`.

#### `agency`
Each of the Plum Book's sections has been split into `agency`. For example, _Arctic Research Commission_, _Department of Homeland Security_ and _NASA_ are classed as agencies. These are each sections within the Plum Books.

#### `year`
The year of Plum Book publication.

### `coordinates`
The value of `location` column translated to geographic coordinates with Google Geocode API.

### Licensing
The data was released by the United States GPO and is therefore classed as information in the Public Domain. See relevant statutes for more information.

### More
The data is also available at [Google Fusion Tables](https://fusiontables.google.com/data?docid=1LRbQQjRDV1uIhUrTvBYH1XzJyWqljKLch_P7P4ua)